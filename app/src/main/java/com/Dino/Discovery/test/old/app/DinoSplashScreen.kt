package com.Dino.Discovery.test.old.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Dino.Discovery.test.old.app.databinding.ActivityDinoSplashScreenBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DinoSplashScreen : AppCompatActivity() {
    private lateinit var binding: ActivityDinoSplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDinoSplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setDinoFullScreen()

        GlobalScope.launch {
            delay(2950)
            val intent = Intent(this@DinoSplashScreen, MainScreen::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setDinoFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}